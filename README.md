# Deployment With Google Kubernates Engine

## Overview:

![Solution Diagram](./diagram/diagram.png)

This practice involves deploying API application using Google Kubernetes Engine (GKE). as you see above I used in this deployment many section:

### Deployment:
The application contains two deployments one for api application called app-api that is an api to get an Article information and it's a stateless application and another for database called mysql and it's a stateful application. 

### Service:
The two deployments have attached services, the first one attached to configure an Internal Load Balancing IP address to act as the frontend to your private backend instances. You do not need a public IP for your load balanced service, and the second one is a cluster ip that exposes the service on a cluster-internal IP.

### Ingress:
The big downside is that each service you expose with a LoadBalancer will get its own IP address, and you have to pay for a LoadBalancer per exposed service, which can get expensive!. for that an ingress here you can expose the services by only one ip address.

### Extensible Service Proxy
I need to log and monitor the api backend, for that I used ESP. In Cloud Endpoints for OpenAPI and gRPC, API requests are relayed through the Extensible Service Proxy, which validates keys and authentication tokens and sends signals (metrics and logs) via the Service Control API. The separation provided by ESP means that you can write the REST in any language, and you can use any framework that supports API description using OpenAPI.

### Autoscaling:
To autoscale application for this scenario I used Horizontal Pod Autoscaler automatically that scales the number of pods in a replication controller, deployment or replica set based on observed CPU utilization (or, with beta support, on some other, application-provided metrics).



## Getting Started:

**Setting the default project:**

To set a default project, run the following command from Cloud Shell:
```
gcloud config set project PROJECT_ID
```
**Setting a default compute zone:**

To set a default compute zone, run the following command:
```
gcloud config set compute/zone COMPUTE_ZONE
```

**Setting a endpoint for ESP:**

To set the Endpoint, run the following command:
```
gcloud endpoints services deploy ./reverse_proxy/openapi.yaml
```

**Setting a GKE:**

Building the cluster by *Google Cloud Deployment Manager* that is an infrastructure deployment service that automates the creation and management of Google Cloud Platform resources for you. Write flexible template and configuration files and use them to create deployments that have a variety of Cloud Platform services, such as Google Cloud Storage, Google Compute Engine, and Google Cloud SQL, configured to work together.

By *Deployment Manager* you can deploy the hole of this infrastracture by one command, but now it just build a cluster for this demo.

To set a cluster, run the following command:

```console
gcloud deployment-manager deployments create kube-sol --config ./deployment_manager/cluster.yaml
```

After creating your cluster, you need to get authentication credentials to interact with the cluster.

To authenticate for the cluster, run the following command:

```console
gcloud container clusters get-credentials CLUSTER_NAME
```
To set namespace, run the following command:

```console
kubectl apply -f my-namespace.yaml
```
To set a mysql deployment, run the following command:

```console
kubectl apply -f ./mysql/mysql-depoyment.yaml
```

To set a app-api deployment, run the following command:

```console
kubectl apply -f ./app/backend-deployment.yaml
```

to set an internal load balancer service, run the following command:

```console
kubectl apply -f ./ingress/loadbalancer-service.yaml
```

to set an ingress, run the following command:

```console
kubectl apply -f ./ingress/api-ingress.yaml
```

to set an HPA, run the following command:

```console
kubectl apply -f ./autoscaling/hpa.yaml
```




